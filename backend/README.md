# TypeScript Full Stack Js Developer assignment
This assignment contains a module which returns a server with the lowest priority. Also create a unit test that can check if the function is running properly.

## Getting started

The command below will install all node_modules, link required packages and start the application.

`npm install`
`npm run start:dev`
`npm run test`

## Files
This includes these files:
* package.json: this file includes the packages to run this project.
* package-lock.json: this file is created for locking the dependency with the installed version.
* tsconfig.json: this file includes typescript configurations.

## Folders

### src
* It includes the typescript source code to run the project