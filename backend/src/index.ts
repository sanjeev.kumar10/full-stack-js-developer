import http from 'http';
import servers from './servers';
const HOST = 'localhost';
const PORT = 5000;

const requestListener = async function (req:any, res:any) {
    if(req.method.toLowerCase() === "get"){
        const response = await servers.findServer();
        res.writeHead(200);
        return res.end(JSON.stringify(response));
    }
    res.writeHead(400);
    return res.end(JSON.stringify({
        msg: "Invalid Request Created"
   }));
};

const server = http.createServer(requestListener);
server.listen(PORT, HOST, () => {
    console.log(`Server is running on http://${HOST}:${PORT}`);
});
