import servers from "./servers";
import fetchMock from 'jest-fetch-mock'
import { Servers } from "./utils/servers";
fetchMock.enableMocks();

jest.setTimeout(30000);

describe("Testing job", () => {
    beforeEach(() => {
        fetchMock.resetMocks();
    })
    it("Is Server online", async () => {
        const server = await servers.findServer();
        expect(server.statusCode).toEqual(200);
    });

    it("Promise for fetching data", async () => {    
        const promiseFetch = await servers.connectToServer(Servers[0]);
        expect(promiseFetch.url).toEqual(Servers[0].url);
    });
});