import fetch from 'node-fetch';
import axios from 'axios';
import { Servers } from './utils/servers';
interface IServerDetails {
    url: string;
    priority: number;
}

class Server {
    servers: any;
    constructor() {
        this.servers = Servers;
    }
    /** 
     * @params :  {}
     * @Description : This method is use to find the online servers.
     * */
    async findServer() {
        const requests = this.servers.map((serverDetail: IServerDetails) =>
            this.connectToServer(serverDetail)
        );
        return Promise.all(requests).then((serverResp) => {
            const activeServers = serverResp.filter(server => server.statusCode >= 200 && server.statusCode <= 299)
            const availableServer = activeServers.reduce((acc, item) => (acc?.priority > item.priority) ? acc : item, null)
            return availableServer ? availableServer : { statusCode: 404, message: 'No server found' };
        });
    }

    /**
     * @param  {IServerDetails} serverDetail
     * @Description : This method is use to return axios promise.
     */
    async connectToServer(serverDetail: IServerDetails) {
        const config = {
            url: serverDetail.url,
            method: 'get',
            timeout: 5000
        }
        return axios(config).then(response => {
            return { statusCode: response.status, priority: serverDetail.priority, url: serverDetail.url }
        }).catch(err => {
            return { statusCode: err.errno, priority: serverDetail.priority, url: serverDetail.url }
        })
    }
}

export default new Server();