
This is a [Next.js](https://nextjs.org/) project bootstrapped with [`create-next-app`](https://github.com/vercel/next.js/tree/canary/packages/create-next-app).

## Getting Started
The command below will install all node_modules, link required packages and start the application.

`npm install`

To run in devlopment mode

`npm run dev`
# or
To run in production mode

`npm run build`

`npm start`

## Blogs
This route contains SSR for getting blogs on server side which uses dummy API.
