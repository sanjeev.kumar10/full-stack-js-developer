import * as React from 'react'
import Breadcrumbs from '@mui/material/Breadcrumbs'
import Typography from '@mui/material/Typography'
import MaterialLink from '@mui/material/Link'
import Stack from '@mui/material/Stack'
import Link from 'next/link'
import styled from '@emotion/styled'

function handleClick(event: React.MouseEvent<HTMLAnchorElement, MouseEvent>) {
	event.preventDefault()
	console.info('You clicked a breadcrumb.')
}

const StyledBreadcrumbs = styled(Breadcrumbs)(() => ({
	'& .MuiBreadcrumbs-li': {
		textTransform: 'uppercase',
		fontSize: 14,
		fontWeight: 600,
		letterSpacing: 1,
		'& .MuiTypography-root': {
			fontSize: 14,
			fontWeight: 600,
		},
	},
	'& .MuiBreadcrumbs-separator': {
		color: '#bf0328',
		paddingBottom: 3,
		fontWeight: 600,
	},
}))

export default function CustomBreadcrumb() {
	const breadcrumbs = [
		<MaterialLink
			underline="hover"
			key="1"
			color="inherit"
			href="/"
			onClick={handleClick}
			component={Link}
		>
			Recepies
		</MaterialLink>,
		<MaterialLink
			underline="hover"
			key="2"
			color="inherit"
			href="/material-ui/getting-started/installation/"
			onClick={handleClick}
			component={Link}
		>
			Bread
		</MaterialLink>,
		<Typography key="3" color="text.primary">
			Quick Bread
		</Typography>,
	]

	return (
		<Stack spacing={3}>
			<StyledBreadcrumbs separator="›" aria-label="breadcrumb">
				{breadcrumbs}
			</StyledBreadcrumbs>
		</Stack>
	)
}
