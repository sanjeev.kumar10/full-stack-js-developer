import * as React from 'react'
import Card from '@mui/material/Card'
import CardHeader from '@mui/material/CardHeader'
import CardContent from '@mui/material/CardContent'
import CardActions from '@mui/material/CardActions'
import Avatar from '@mui/material/Avatar'
import IconButton from '@mui/material/IconButton'
import Typography from '@mui/material/Typography'
import { red } from '@mui/material/colors'
import FavoriteIcon from '@mui/icons-material/Favorite'
import ShareIcon from '@mui/icons-material/Share'
import Image from 'next/image'

export default function BlogView({ post }: any) {
	return (
		<Card sx={{ maxWidth: 345 }}>
			<CardHeader
				avatar={
					<Avatar sx={{ bgcolor: red[500] }} aria-label="recipe">
						{post?.brand?.charAt(0)}
					</Avatar>
				}
				title={post?.title}
				subheader={`${post?.brand} | ${post?.category}`}
				sx={{
					textTransform: 'capitalize',
				}}
			/>
			<Image src={post?.thumbnail} alt={post?.title} height="194" width="345" />
			<CardContent>
				<Typography variant="body2" color="text.secondary">
					{post?.description}
				</Typography>
			</CardContent>
			<CardActions disableSpacing>
				<IconButton aria-label="add to favorites">
					<FavoriteIcon />
				</IconButton>
				<IconButton aria-label="share">
					<ShareIcon />
				</IconButton>
			</CardActions>
		</Card>
	)
}
