import Navbar from '../navbar'
import Meta from '../meta'
import { styled } from '@mui/system'
type LayoutProps = {
	children: React.ReactNode
}
const StyledMain = styled('main')({
	backgroundColor: '#fff',
	minHeight: '100vh',
	minWidth: '100vw',
})
const Footer = styled('div')({
	textAlign: 'center',
	background: '#bf032811',
	minWidth: '100vw',
	padding: '10px 0',
})

const MainContent = styled('div')({
	paddingTop: '116px',
})
import SubNavBar from '@components/subNavBar'
export default function PublicLayout({ children }: LayoutProps) {
	return (
		<>
			<Meta />
			<StyledMain>
				<Navbar />
				<SubNavBar />
				<MainContent sx={{paddingTop: {
                    xs: "70px"
                }}}>{children}</MainContent>
				<Footer>All Rights Reserved 2022</Footer>
			</StyledMain>
		</>
	)
}
