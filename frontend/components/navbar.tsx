import Link from 'next/link'
import * as React from 'react'
import Toolbar from '@mui/material/Toolbar'
import MenuIcon from '@mui/icons-material/Menu'
import ChevronRightIcon from '@mui/icons-material/ChevronRight'
import {
	Container,
	Drawer,
	IconButton,
	Link as Navlink,
	List,
	ListItem,
	ListItemButton,
	ListItemText,
	styled,
} from '@mui/material'
import { useRouter } from 'next/router'
import { Logo, LogoContainer, LogoSmall } from '@components/style'
import profilePic from '@public/logo.jpg'
import AdbIcon from '@mui/icons-material/Adb'

interface Props {
	window?: () => Window
}

const StyledLink = styled(Link)(({ prop }: any) => ({
	borderBottom: prop ? '3px solid transparent' : 'none',
	'&.MuiLink-root': {
		textDecoration: 'none',
		padding: '0px 2px 2px',
		margin: '0 20px',
		fontSize: '16px',
		color: '#000000',
		'@media(max-width: 599px)': {
			margin: '0 10px',
			display: 'none',
		},
		':hover': {
			borderBottom: '3px solid #bf0328',
		},
		':first-of-type': {
			marginLeft: '110px !important',
			'@media(max-width: 991px)': {
				marginLeft: '90px !important',
			},
			'@media(max-width: 576px)': {
				marginLeft: '40px !important',
			},
		},
	},
}))

const MainNavbar = styled('div')({
	justifyContent: 'space-between',
	'@media(max-width: 599px)': {
		display: 'flex',
		justifyContent: 'end',
	},
})
const DrawerFooter = styled('div')(({ theme }) => ({
	display: 'flex',
	alignItems: 'center',
	padding: theme.spacing(0, 1),
	justifyContent: 'start',
	position: 'fixed',
	bottom: 0,
	boxShadow: '0 0 10px #00000011',
	width: '100%',
}))

const NAV_MENUS = [
	{
		name: 'Home',
		href: '/',
	},
	{
		name: 'Recepies',
		href: '/recepies',
	},
	{
		name: 'Shop',
		href: '/shop',
		class: 'active',
	},
	{
		name: 'About',
		href: '/about',
	},
	{
		name: 'Blog',
		href: '/blog',
	},
]
export default function Navbar(props: Props) {
	const route = useRouter()

	const { window } = props
	const [mobileOpen, setMobileOpen] = React.useState(false)

	const handleDrawerToggle = () => {
		setMobileOpen(!mobileOpen)
	}

	const container = window !== undefined ? () => window().document.body : undefined

	const drawer = (
		<div>
			<List>
				{NAV_MENUS.map((menu, index) => (
					<ListItem key={index} disablePadding>
						<ListItemButton sx={{ display: 'block' }}>
							<Link href={menu.href}>
								<ListItemText
									primary={menu.name}
									sx={{ textTransform: 'capitalize', display: 'block' }}
								/>
							</Link>
						</ListItemButton>
					</ListItem>
				))}
			</List>
		</div>
	)

	return (
		<>
			<Toolbar
				component="nav"
				variant="dense"
				sx={{
					minHeight: '60px',
					position: 'fixed',
					top: 0,
					zIndex: 11,
					background: '#fff',
					width: '100%',
					boxShadow: '0 0 10px #00000011',
				}}
			>
				<LogoSmall src={profilePic} alt="" sx={{ display: { xs: 'block', sm: 'none', md: 'none', lg: 'none', xl: 'none' }, mr: 1 }}/>
				<Container maxWidth="lg">
					<LogoContainer
						sx={{
							display: {
								xs: 'none',
								sm: 'block',
								md: 'block',
								lg: 'block',
								xl: 'block',
							},
						}}
					>
						<Logo src={profilePic} alt="" />
					</LogoContainer>
					<MainNavbar>
						{NAV_MENUS.map((menu, index) => (
							<Navlink
								key={index}
								component={StyledLink}
								color="inherit"
								noWrap
								href={menu.href}
								variant="body2"
								sx={{
									borderBottom: route?.asPath === menu.href ? '3px solid #bf0328' : 'none',
								}}
							>
								{menu.name}
							</Navlink>
						))}
						<IconButton
							color="inherit"
							aria-label="open drawer"
							edge="end"
							onClick={handleDrawerToggle}
							sx={{ display: { sm: 'none' } }}
						>
							<MenuIcon />
						</IconButton>
						<Drawer
							container={container}
							variant="temporary"
							anchor="right"
							open={mobileOpen}
							onClose={handleDrawerToggle}
							ModalProps={{
								keepMounted: true,
							}}
							sx={{
								display: { xs: 'block', sm: 'none' },
								'& .MuiDrawer-paper': {
									boxSizing: 'border-box',
									width: 250,
								},
							}}
						>
							<DrawerFooter>
								<IconButton onClick={handleDrawerToggle}>
									<ChevronRightIcon />
								</IconButton>
							</DrawerFooter>
							{drawer}
						</Drawer>
					</MainNavbar>
				</Container>
			</Toolbar>
		</>
	)
}
