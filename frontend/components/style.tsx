import { Button } from '@mui/material'
import { styled } from '@mui/system'
import Image from 'next/image'
import Link from 'next/link'

export const HeadNavigation = styled('div')({
	display: 'flex',
})
export const MainContainer = styled('div')({
	padding: '12px 0 7px',
	background: '#f9f5f0',
	display: 'flex',
})
export const LogoContainer = styled('div')({
	position: 'relative',
	width: 80,
})
export const Logo = styled(Image)({
	height: 110,
	width: 80,
	position: 'absolute',
	left: 0,
	top: -15,
})
export const LogoSmall = styled(Image)({
	height: 40,
	width: 40,
})
export const StyledLink = styled(Link)(() => ({
	color: '#000',
	textTransform: 'uppercase',
	fontSize: 14,
	letterSpacing: 1,
	textDecoration: 'none',
}))

export const Section = styled('div')({
	padding: '60px 0px',
})

export const ItemImageWrapper = styled('div')({
	width: '100%',
	textAlign: 'center',
	paddingLeft: '10px',
	img: {
		maxWidth: '100%',
		width: '100%',
	},
})

export const LeftWrapper = styled('div')({
	display: 'flex',
	flexDirection: 'column',
	justifyContent: 'space-between',
	height: '100%',
	padding: 10,
})

export const MainHeading = styled('h1')({
	padding: '10px 0 20px',
	fontSize: 40,
    fontFamily: "Times New Roman",
})

export const ItemDetail = styled('p')({
	padding: '12px 0px 0',
	fontSize: "16px",
	wordSpacing: 3,
    lineHeight: "22px",
	color: '#000000bf',
    letterSpacing: "1px"
	// fontFamily: 'brandon-grotesque',
})
export const Bold = styled('div')({
	fontWeight: 600,
	padding: '4px 0px 0px 6px',
	color: '#000000e3',
})
export const Label = styled('div')({
	fontWeight: 600,
	fontSize: "12px",
	padding: '4px 0px 0px 6px',
	color: '#000000ad',
    letterSpacing: "1px"
})
export const Timing = styled('div')({
	display: 'flex',
	padding: '10px 0',
	'@media(max-width: 475px)': {
		flexWrap: 'wrap',
	},
})
export const Flex = styled('div')({
	display: 'flex',
	alignItems: 'center',
	gap: 15,
	flexWrap: 'wrap',
})
export const YieldIcon = styled('div')({
	border: '3.5px solid #000000',
	width: 35,
	borderRadius: '50%',
	display: 'flex',
	justifyContent: 'center',
	alignItems: 'center',
	height: 37,
	marginRight: 5,
})
export const ButtonContainer = styled('div')({
	display: 'flex',
	gap: 10,
	padding: '20px 10px 10px',
})
export const Footer = styled('div')({
	paddingTop: '5px',
	margin: '30px 0px 10px',
	borderTop: '1px solid #ddd',
})

export const CustomButton = styled(Button)(() => ({
	lineHeight: '24px',
	whiteSpace: 'nowrap',
}))
export const Banner = styled('div')({
	height: '100px',
	backgroundColor: '#bf032811',
	fontSize: 56,
	fontWeight: 700,
	color: '#000',
	display: 'flex',
	justifyContent: 'center',
	alignItems: 'center',
	position: 'relative',
})