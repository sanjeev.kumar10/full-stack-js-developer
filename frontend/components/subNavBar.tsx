import React from 'react'
import { HeadNavigation, StyledLink } from './style'
import { Container } from '@mui/material'

export default function subNavBar() {
	return (
		<Container
			maxWidth="xl"
			sx={{
				display: {
					xs: 'none',
					sm: 'flex',
					md: 'flex',
					lg: 'flex',
					xl: 'flex',
				},
				position: 'fixed',
				padding: '12px 0 7px',
				background: '#f9f5f0',
				top: '60px',
				zIndex: 10,
				maxWidth: 'unset !important',
			}}
		>
			<HeadNavigation
				sx={{
					position: 'relative',
					left: {
						xs: '16%',
						sm: '16%',
						md: '16%',
						lg: '16%',
						xl: '16%',
					},
                    '@media(min-width: 1500px)': {
                        left: '18%',
                    },
                    '@media(min-width: 1700px)': {
                        left: '22%',
                    },
                    '@media(min-width: 2000px)': {
                        left: '30%',
                    },
				}}
			>
				<StyledLink
					href="#"
					sx={{
						padding: '10px 10px 10px 30px',
					}}
				>
					{'Catageories'}
				</StyledLink>
				<StyledLink
					href="#"
					sx={{
						padding: '10px',
					}}
				>
					{'Collections'}
				</StyledLink>
				<StyledLink
					href="#"
					sx={{
						padding: '10px',
					}}
				>
					{'Resources'}
				</StyledLink>
			</HeadNavigation>
		</Container>
	)
}
