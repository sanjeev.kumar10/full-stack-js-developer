import PublicLayout from '@components/layouts/public'
import { styled } from '@mui/system'
import Image from 'next/image'
import NotFound404 from '../public/404.jpg'
export const MainContainer = styled('div')({
	height: '100vh',
	width: '100vw',
	display: 'flex',
	flexDirection: 'column',
	alignItems: 'center',
	justifyContent: 'center',
	gap: '20px',
	fontSize: '30px',
	fontWeight: '700',
})

export default function NotFound() {
	return (
		<>
			<PublicLayout>
				<MainContainer>
					<Image src={NotFound404} alt="" />
					Page not Found!
				</MainContainer>
			</PublicLayout>
		</>
	)
}
