import React from 'react'
import PublicLayout from '@components/layouts/public'
import { Banner, ItemDetail, MainHeading, Section } from '@components/style'
import Link from 'next/link'
import { Container, styled } from '@mui/material'

export const StyledLink = styled(Link)({
	position: 'absolute',
	bottom: '15px',
	zIndex: '999',
	svg: {
		height: 40,
		width: 40,
		border: '2px solid #000',
		borderRadius: '50%',
	},
})
export default function About() {
	return (
		<PublicLayout>
			<Banner>
				OUR STORY
				<StyledLink href="#whoWeAre"></StyledLink>
				<svg
					xmlns="http://www.w3.org/2000/svg"
					viewBox="0 0 1000 100"
					preserveAspectRatio="none"
					style={{
						position: 'absolute',
						bottom: 0,
						fill: '#00000012',
						transform: 'rotate(180deg)',
					}}
				>
					<path d="M194,99c186.7,0.7,305-78.3,306-97.2c1,18.9,119.3,97.9,306,97.2c114.3-0.3,194,0.3,194,0.3s0-91.7,0-100c0,0,0,0,0-0 L0,0v99.3C0,99.3,79.7,98.7,194,99z"></path>
				</svg>
			</Banner>
			<Section sx={{ textAlign: 'center' }}>
				<Container maxWidth="lg">
					<MainHeading id="whoWeAre">WHO WE ARE</MainHeading>
					<ItemDetail>
						Lorem ipsum dolor sit amet consectetur, adipisicing elit. Officiis labore nulla ratione
						perspiciatis rem expedita, id nam, reprehenderit provident fuga odio vel minus?
						Corporis, quae! Obcaecati ullam commodi at nihil? Vero molestiae quos blanditiis quas
						fugiat architecto quam soluta optio dicta, fugit expedita nesciunt error temporibus
						perferendis laboriosam dolor dolorum eos doloribus! Vitae officia rem doloremque,
						numquam fugit at. Repudiandae accusantium quisquam dolorem sequi quos. Vel dolore veniam
						in nobis dolorem tempore voluptatem mollitia officiis rem quos, laudantium quasi a
						temporibus eveniet necessitatibus voluptatum aperiam soluta quo optio sunt saepe nisi
						iusto delectus neque.
					</ItemDetail>
					<ItemDetail>
						{' '}
						Voluptate vel voluptatum dolor earum in rerum tempora odio eum fuga ea unde impedit
						tempore numquam aspernatur expedita consequuntur aliquid quasi dolore, sapiente eaque
						maxime hic? Unde dolorum incidunt aliquid voluptate aspernatur dicta impedit distinctio
						odio nisi porro autem facere ex culpa, est eum. Qui, quod laudantium. Consequatur
						delectus aperiam qui ab iusto? Cumque quod est debitis id eveniet quidem esse recusandae
						mollitia inventore at quibusdam ea distinctio, modi porro vitae optio? Provident
						reprehenderit vero autem incidunt consequuntur inventore blanditiis officiis asperiores
						obcaecati placeat at corrupti, alias earum, quos, dolorum dolorem atque ipsum ut neque!
						Earum enim ratione cupiditate possimus magni? Sequi eos fugit accusamus enim possimus
						labore sunt odio itaque quasi ea repudiandae dolorum ad impedit qui quisquam voluptas
						ducimus, cum dolores inventore quam sapiente laboriosam! Ea ducimus nobis natus culpa
						dolorem odio molestiae voluptates dolor in dolores sed impedit, officia ipsa.
					</ItemDetail>
				</Container>
			</Section>

			<Section sx={{ textAlign: 'center', background: '#ebebeb' }}>
				<Container maxWidth="lg">
					<MainHeading
                    >OUR MISSION</MainHeading>
					<ItemDetail sx={{ fontSize: '18px', lineHeight: "0px", letterSpacing: "0px" }}>
						To inspire and teach families to cook and eat real food together
					</ItemDetail>
				</Container>
			</Section>
		</PublicLayout>
	)
}
