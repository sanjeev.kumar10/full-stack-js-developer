import React from 'react'
import PublicLayout from '@components/layouts/public'
import { Banner, Section } from '../../components/style'
import { StyledLink } from 'pages/about'
import BlogViewCard from '@components/card/blogView'
import { Container, styled } from '@mui/material'

const BlogWrapper = styled('div')({
	display: 'flex',
	gap: '40px',
	flexWrap: 'wrap',
	justifyContent: 'center',
})

export default function Blogs({ posts }: any) {
	return (
		<PublicLayout>
			<Banner>
				Blogs
				<StyledLink href="#blogs">{/* <ExpandMore /> */}</StyledLink>
				<svg
					xmlns="http://www.w3.org/2000/svg"
					viewBox="0 0 1000 100"
					preserveAspectRatio="none"
					style={{
						position: 'absolute',
						bottom: 0,
						fill: '#00000012',
						transform: 'rotate(180deg)',
					}}
				>
					<path d="M194,99c186.7,0.7,305-78.3,306-97.2c1,18.9,119.3,97.9,306,97.2c114.3-0.3,194,0.3,194,0.3s0-91.7,0-100c0,0,0,0,0-0 L0,0v99.3C0,99.3,79.7,98.7,194,99z"></path>
				</svg>
			</Banner>

			<Section>
				<Container maxWidth="lg">
					<BlogWrapper id="blogs">
						{posts.map((post: any) => {
							return <BlogViewCard key={post.id} post={post} />
						})}
					</BlogWrapper>
				</Container>
			</Section>
		</PublicLayout>
	)
}

export async function getServerSideProps() {
	const postsRsp = await fetch('https://dummyjson.com/products')
	const posts = await postsRsp.json()
	return {
		props: { posts: posts?.products || [] }, // will be passed to the page component as props
	}
}
