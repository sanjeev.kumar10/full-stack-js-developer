import React from 'react'
import PublicLayout from '@components/layouts/public'
import Breadcrumbs from '@components/breadcrum/custom'
import { Container, Grid } from '@mui/material'
import AccessTimeIcon from '@mui/icons-material/AccessTime'
import ScatterPlotIcon from '@mui/icons-material/ScatterPlot'
import AddIcon from '@mui/icons-material/Add'
import PrintIcon from '@mui/icons-material/Print'
import Image from 'next/image'
import ItemImage from '@public/Baked-potato.webp'
import {
	CustomButton,
	Footer,
	ButtonContainer,
	YieldIcon,
	Flex,
	Timing,
	Label,
	Bold,
	ItemDetail,
	MainHeading,
	LeftWrapper,
	ItemImageWrapper,
	Section,
} from '@components/style'

export default function Recepies() {
	return (
		<PublicLayout>
			<Container maxWidth="lg">
				<Section>
					<Grid container spacing={2}>
						<Grid item md={5}>
							<LeftWrapper>
								<div>
									<Breadcrumbs />
									<MainHeading>Classic Mini Baked Potato</MainHeading>
								</div>
								<ItemDetail>
									This easy recipe for Mini Baked Potatoes makes a great appetizer for any party or
									game day! Roasted baby potatoes are loaded up with sour cream and chives. They are
									a great finger food that everyone will love. Mini Baked Potatoes turn the classic
									popular flavor of baked potatoes into a bite sized appetizer.
									<br />
                                    They are easy to make and perfect for any event. Stuff the baked mini potatoes with
									sour cream and chives or your favorite toppings and serve on game day or for a
									party.
								</ItemDetail>
							</LeftWrapper>
						</Grid>
						<Grid item md={7}>
							<ItemImageWrapper>
								<Image src={ItemImage} alt="Image" height={400} />
							</ItemImageWrapper>
						</Grid>
						<Grid item md={5}>
							<Flex>
								<Timing>
									<AccessTimeIcon style={{ fontSize: '45px' }} />
									<div>
										<Label>PREP</Label>
										<Bold> 15 mins</Bold>
									</div>
								</Timing>
								<div>
									<Label>BAKE</Label>
									<Bold> 1 hour to 1 hour 30 mins</Bold>
								</div>
								<div>
									<Label>TOTAL</Label>
									<Bold> 1 hr 20 mins</Bold>
								</div>
							</Flex>
							<Footer>
								<Flex>
									<Timing>
										<YieldIcon>
											<ScatterPlotIcon sx={{
                                                fontSize: "29px"
                                            }} />
										</YieldIcon>
										<div>
											<Label>YIELD</Label>
											<Bold
												sx={{
													minWidth: 165,
												}}
											>
												1 loaf 20 generous serving
											</Bold>
										</div>
										<ButtonContainer>
											<CustomButton variant="outlined" startIcon={<AddIcon />} color="error">
												Save Recipe
											</CustomButton>
											<CustomButton
												variant="outlined"
												startIcon={<PrintIcon />}
												color="error"
												onClick={() => {}}
											>
												Print
											</CustomButton>
										</ButtonContainer>
									</Timing>
								</Flex>
							</Footer>
						</Grid>
					</Grid>
				</Section>
			</Container>
		</PublicLayout>
	)
}
