import React from 'react'
import PublicLayout from '@components/layouts/public'
import Breadcrumbs from '@components/breadcrum/custom'
import { Container, Grid } from '@mui/material'
import AccessTimeIcon from '@mui/icons-material/AccessTime'
import ScatterPlotIcon from '@mui/icons-material/ScatterPlot'
import AddIcon from '@mui/icons-material/Add'
import PrintIcon from '@mui/icons-material/Print'
import Image from 'next/image'
import ItemImage from '@public/bread.jpg'

import {
	CustomButton,
	Footer,
	ButtonContainer,
	YieldIcon,
	Flex,
	Timing,
	Label,
	Bold,
	ItemDetail,
	MainHeading,
	LeftWrapper,
	ItemImageWrapper,
	Section,
} from '@components/style'

const print = () => {
	window.print()
}
export default function Shop() {
	return (
		<PublicLayout>
			<Container maxWidth="lg">
				<Section>
					<Grid container spacing={2}>
						<Grid item md={5}>
							<LeftWrapper>
								<div>
									<Breadcrumbs />
									<MainHeading>Whole-Grain Banana Bread</MainHeading>
								</div>
								<ItemDetail>
									This one-bowl banana bread-our 2018 Recipe of the Year- uses the simplest
									ingredients, but is incredibly moist and flavorful. While the recipe calls for a
									50/50 mix of flours (all-purpose and whole wheat), we often make the bread 100%
									whole wheat, and honestly? No one can tell, it is that good! And not only is this
									bread delicious-it is versatile.
								</ItemDetail>
							</LeftWrapper>
						</Grid>
						<Grid item md={7}>
							<ItemImageWrapper>
								<Image src={ItemImage} alt="Image" height={400} />
							</ItemImageWrapper>
						</Grid>
						<Grid item md={5}>
							<Flex>
								<Timing>
									<AccessTimeIcon style={{ fontSize: '45px' }} />
									<div>
										<Label>PREP</Label>
										<Bold> 10 mins</Bold>
									</div>
								</Timing>
								<div>
									<Label>BAKE</Label>
									<Bold> 1 hr to 1 hr 15 mins</Bold>
								</div>
								<div>
									<Label>TOTAL</Label>
									<Bold> 1 hr 10 mins</Bold>
								</div>
							</Flex>
							<Footer>
								<Flex>
									<Timing>
										<YieldIcon>
											<ScatterPlotIcon />
										</YieldIcon>
										<div>
											<Label>YIELD</Label>
											<Bold
												sx={{
													width: 165,
												}}
											>
												1 loaf 12 generous serving
											</Bold>
										</div>
										<ButtonContainer>
											<CustomButton variant="outlined" startIcon={<AddIcon />} color="error">
												Save Recipe
											</CustomButton>
											<CustomButton
												variant="outlined"
												startIcon={<PrintIcon />}
												color="error"
												onClick={print}
											>
												Print
											</CustomButton>
										</ButtonContainer>
									</Timing>
								</Flex>
							</Footer>
						</Grid>
					</Grid>
				</Section>
			</Container>
		</PublicLayout>
	)
}
